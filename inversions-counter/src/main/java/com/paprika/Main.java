package com.paprika;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

/**
 * Created by akapitonov on 1/19/2015.
 */
public class Main {

    private static int[] readFromFile(String fileName) {
        List<String> tmp = null;

        try {
            tmp = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int[] result = null;

        if (tmp != null) {
            result = new int[tmp.size()];
            for (int i = 0; i < result.length; i++) {
                result[i] = Integer.parseInt(tmp.get(i));

            }
        }

        return result;
    }

    public static void main(String[] args) {
        InversionsCounter ic = new InversionsCounter();
        int[] test = readFromFile("IntegerArray.txt");
        //System.out.println("inversions number: " + inversionsCounter.countInversions(test));
        long result = 0;
        long time0 = System.currentTimeMillis();
        result = ic.invCount(test);
        long time = System.currentTimeMillis() - time0;
        System.out.println("inversions number : " + result + ", working time : " + time + " ms");
    }
}
