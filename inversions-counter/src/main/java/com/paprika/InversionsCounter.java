package com.paprika;

/**
 * The modified merge sort algorithm which counts number of the inversions in the array
 */
public class InversionsCounter {

    public long invCount(int[] a) {
        int[] aux = new int[a.length];
        return invCount(a, aux, 0, a.length - 1);
    }

    private long invCount(int[] a, int[] aux, int lo, int hi) {
        if (hi <= lo) return 0;
        int mid = lo + (hi - lo) / 2;
        return invCount(a, aux, lo, mid) + invCount(a, aux, mid + 1, hi) + merge(a, aux, lo, mid, hi);
    }

    private long merge(int[] a, int[] aux, int lo, int mid, int hi) {
        int i = lo;
        int j = mid + 1;
        long count = 0;

        System.arraycopy(a, 0, aux, 0, a.length);

        for (int k = lo; k <= hi; k++) {
            if (i > mid) {
                a[k] = aux[j];
                count += k - j;
                j++;
            } else if (j > hi) {
                a[k] = aux[i];
                count += k - i;
                i++;
            } else if (aux[j] <= aux[i]) {
                a[k] = aux[j];
                j++;
            } else {
                a[k] = aux[i];
                count += k - i;
                i++;
            }
        }

        return count;
    }

    public void printArray(int[] a) {
        for (int i : a) {
            System.out.print(i + " ");
        }
        System.out.println("\n");
    }
}
