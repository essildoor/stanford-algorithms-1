package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class MedianThreePicker implements PivotPicker {

    @Override
    public int pickPivot(int[] a, int l, int r) {
        final int first = a[l];
        final int middleIndex = l + (r - l) / 2;
        final int second = a[middleIndex];
        final int third = a[r];

        int max = first;
        if (max < second) max = second;
        if (max < third) max = third;

        if (max == first) {
            return second > third ? middleIndex : r;
        } else if (max == second) {
            return first > third ? l : r;
        } else {
            return first > second ? l : middleIndex;
        }
    }
}
