package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public interface PivotPicker {

    public int pickPivot(int[] a, int l, int r);
}
