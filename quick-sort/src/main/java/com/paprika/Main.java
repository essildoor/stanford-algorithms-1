package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class Main {

    private static void printArray(int[] a) {
        for (int i : a) {
            System.out.println(i);
        }
    }



    public static void main(String[] args) {
        int[] a = Util.readFromFile("QuickSort.txt");

        QuickSorter sorter = new QuickSorter(new MedianThreePicker());

        System.out.println(sorter.sort(a));
    }
}
