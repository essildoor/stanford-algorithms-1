package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class FirstElementPicker implements PivotPicker {

    @Override
    public int pickPivot(int[] a, int l, int r) {
        return l;
    }
}
