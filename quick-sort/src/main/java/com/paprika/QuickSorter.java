package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class QuickSorter {

    private PivotPicker pivotPicker;
    private int comparesCount;

    public QuickSorter(PivotPicker pivotPicker) {
        this.pivotPicker = pivotPicker;
        this.comparesCount = 0;
    }

    public int sort(int[] a) {
        sort(a, 0, a.length - 1);
        int result = comparesCount;
        comparesCount = 0;
        return result;
    }

    private void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    private int partition(int[] a, int l, int r) {
        int p = a[l];
        int i = l + 1;
        for (int j = l + 1; j <= r; j++) {
            if (a[j] < p) {
                swap(a, j, i);
                i++;
            }
        }
        swap(a, l, i - 1);
        return i - 1;
    }

    private void sort(int[] a, int l, int r) {
        if (l == r) return;
        int pivotIndex = pivotPicker.pickPivot(a, l, r);
        if (pivotIndex != l) swap(a, pivotIndex, l);
        comparesCount += r - l;
        pivotIndex = partition(a, l, r);
        if (pivotIndex == l) {
            sort(a, l + 1, r);
        } else if (pivotIndex == r) {
            sort(a, l, r - 1);
        } else {
            sort(a, l, pivotIndex - 1);
            sort(a, pivotIndex + 1, r);
        }
    }
}
