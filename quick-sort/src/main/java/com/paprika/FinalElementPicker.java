package com.paprika;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class FinalElementPicker implements PivotPicker {

    @Override
    public int pickPivot(int[] a, int l, int r) {
        return r;
    }
}
