package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class Util {
    public static int[] readFromFile(String fileName) {
        List<String> tmp = null;

        try {
            tmp = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int[] result = null;

        if (tmp != null) {
            result = new int[tmp.size()];
            for (int i = 0; i < result.length; i++) {
                result[i] = Integer.parseInt(tmp.get(i));

            }
        }

        return result;
    }
}
