package com.paprika;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akapitonov on 2/9/2015.
 */
public class QuickSorterTest {

    @Test
    public void testFirst() throws Exception {
        QuickSorter sorter = new QuickSorter(new FirstElementPicker());
        int[] a = Util.readFromFile("10.txt");
        int[] b = Util.readFromFile("100.txt");
        int[] c = Util.readFromFile("1000.txt");

        Assert.assertEquals(25, sorter.sort(a));
        Assert.assertEquals(615, sorter.sort(b));
        Assert.assertEquals(10297, sorter.sort(c));
    }

    @Test
    public void testFinal() throws Exception {
        QuickSorter sorter = new QuickSorter(new FinalElementPicker());
        int[] a = Util.readFromFile("10.txt");
        int[] b = Util.readFromFile("100.txt");
        int[] c = Util.readFromFile("1000.txt");

        Assert.assertEquals(29, sorter.sort(a));
        Assert.assertEquals(587, sorter.sort(b));
        Assert.assertEquals(10184, sorter.sort(c));
    }

    @Test
    public void testMedian() throws Exception {
        QuickSorter sorter = new QuickSorter(new MedianThreePicker());
        int[] a = Util.readFromFile("10.txt");
        int[] b = Util.readFromFile("100.txt");
        int[] c = Util.readFromFile("1000.txt");

        Assert.assertEquals(21, sorter.sort(a));
        Assert.assertEquals(518, sorter.sort(b));
        Assert.assertEquals(8921, sorter.sort(c));
    }
}
