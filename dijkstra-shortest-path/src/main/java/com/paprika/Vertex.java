package com.paprika;

import java.util.Map;

/**
 * Created by akapitonov on 2/26/2015.
 */
public class Vertex {
    private int number;
    private Map<Integer, Integer> adjacencyMap;
    private boolean isVisited;
    private int cost;

    public Vertex() {
    }

    public Vertex(int number, Map<Integer, Integer> adjacencyMap) {
        this.number = number;
        this.adjacencyMap = adjacencyMap;
        this.cost = Integer.MAX_VALUE;
    }

    public int getNumber() {
        return number;
    }

    public Map<Integer, Integer> getAdjacencyMap() {
        return adjacencyMap;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean isVisited) {
        this.isVisited = isVisited;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
