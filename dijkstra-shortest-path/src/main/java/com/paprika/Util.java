package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by akapitonov on 2/27/2015.
 */
public class Util {
    public static Map<Integer, Vertex> readFromFile(String fileName) throws IOException {
        List<String> tmp = null;

        try {
            tmp = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tmp == null || tmp.size() == 0) {
            throw new IOException("Empty file");
        }

        StringTokenizer st;
        final String DELIMITER = " \t";
        final String WEIGHT_DELIMITER = ",";
        Map<Integer, Vertex> result = new HashMap<Integer, Vertex>();
        try {
            for (String line : tmp) {
                st = new StringTokenizer(line, DELIMITER);
                Integer vertexNumber = Integer.parseInt(st.nextToken());
                Map<Integer, Integer> adjacencyMap = new HashMap<Integer, Integer>();
                while (st.hasMoreTokens()) {
                    final String edge = st.nextToken();
                    final StringTokenizer anotherOneSt = new StringTokenizer(edge, WEIGHT_DELIMITER);
                    final Integer targetVertexNumber = Integer.parseInt(anotherOneSt.nextToken());
                    final String weight = anotherOneSt.nextToken();
                    adjacencyMap.put(targetVertexNumber, Integer.parseInt(weight));
                }
                result.put(vertexNumber, new Vertex(vertexNumber, adjacencyMap));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
