package com.paprika;

import java.io.IOException;
import java.util.*;

/**
 * Created by akapitonov on 2/26/2015.
 */
public class DijkstraPathSearch {

    private class VertexComparator implements Comparator<Vertex> {

        @Override
        public int compare(Vertex o1, Vertex o2) {
            return o1.getCost() - o2.getCost();
        }
    }

    public Map<Integer, Integer> getPath(Map<Integer, Vertex> graph, int startVertex) {
        final Queue<Vertex> unvisited = new PriorityQueue<Vertex>(graph.size(), new VertexComparator());
        final Map<Integer, Integer> pathList = new HashMap<Integer, Integer>(graph.size());
        unvisited.addAll(graph.values());
        while (!unvisited.isEmpty()) {
            Vertex currentVertex = unvisited.poll();
            for (Integer vertexNumber : currentVertex.getAdjacencyMap().keySet()) {
                Vertex v = graph.get(vertexNumber);
                int newCost = currentVertex.getCost() + currentVertex.getAdjacencyMap().get(vertexNumber);
                if (v.getCost() > newCost) {
                    unvisited.remove(v);
                    v.setCost(newCost);
                    unvisited.add(v);
                    pathList.put(vertexNumber, currentVertex.getNumber());
                }
            }
        }
        return pathList;
    }

    /**
     * prepares graph for Dijkstra search routine
     *
     * @param graph       graph
     * @param startVertex start vertex number
     */
    private static void setupGraph(Map<Integer, Vertex> graph, int startVertex) {
        for (int i : graph.keySet()) {
            if (i == startVertex) {
                graph.get(i).setCost(0);
            } else {
                graph.get(i).setCost(Integer.MAX_VALUE / 2);
            }
        }
    }

    private static List<Integer> reconstructPath(Map<Integer, Integer> paths, Integer start, Integer end) {
        List<Integer> result = new LinkedList<Integer>();
        Integer current = end;
        while (!current.equals(start)) {
            result.add(current);
            current = paths.get(current);
        }
        return result;
    }

    private static void printPath(Map<Integer, Vertex> graph, List<Integer> path, Integer endVertex) {
        System.out.print(endVertex + " : ");
        System.out.print("[ ");
        @SuppressWarnings("unchecked") Iterator<Integer> it = ((Deque<Integer>) path).descendingIterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }
        System.out.print("]");
        System.out.print("  path length: " + graph.get(endVertex).getCost() + "\n");
    }

    private static void printPaths(Map<Integer, Vertex> graph,
                                   Map<Integer, Integer> paths,
                                   Integer start,
                                   Collection<Integer> vertexCollection) {
        for (Integer endVertex : vertexCollection) {
            printPath(graph, reconstructPath(paths, start, endVertex), endVertex);
        }
    }

    private static String getSpecifiedPathsLengths(Map<Integer, Vertex> graph, Collection<Integer> vertexCollection) {
        String result = "";
        for (Integer endVertex : vertexCollection) {
            result += graph.get(endVertex).getCost();
            result += ",";
        }
        return result.substring(0, result.length() - 1);
    }

    public static void main(String[] args) throws IOException {
        DijkstraPathSearch dps = new DijkstraPathSearch();
        Map<Integer, Vertex> graph = Util.readFromFile("dijkstraData.txt");
        Integer startVertex = 1;
        long timeStart = System.currentTimeMillis();
        setupGraph(graph, startVertex);
        Map<Integer, Integer> result = dps.getPath(graph, startVertex);
        long time = System.currentTimeMillis() - timeStart;
        List<Integer> targetVertexes = Arrays.asList(7,37,59,82,99,115,133,165,188,197);
        printPaths(graph, result, startVertex, targetVertexes);
        System.out.println();
        System.out.println(getSpecifiedPathsLengths(graph, targetVertexes));
        System.out.println("Calc time: " + time + " ms");
    }
}
