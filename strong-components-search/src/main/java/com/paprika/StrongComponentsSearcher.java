package com.paprika;

import java.io.IOException;
import java.util.*;

/**
 *
 * Created by akapitonov on 2/23/2015.
 */
public class StrongComponentsSearcher {

    private int t;
    private int s;
    boolean isFirstPass;
    private Map<Integer, Integer> leader;
    private Map<Integer, Integer> f;

    public void dfsLoop(Digraph g) {
        for (Iterator<Integer> it = isFirstPass ? g.getBackwardVertexIterator() : Util.getDescendingIterator(f.keySet()); it.hasNext();) {
            int tmp = it.next();
            int i = isFirstPass ? tmp : f.get(tmp);
            if (!g.isExplored(i)) {
                if (!isFirstPass) {
                    s = tmp;
                }
                dfs(g, i);
            }
        }
    }

    public void dfs(Digraph g, int i) {
        g.setExplored(i, true);
        if (!isFirstPass) {
            if (leader.get(s) == null) {
                leader.put(s, 1);
            } else {
                int updated = leader.get(s) + 1;
                leader.put(s, updated);
            }
        }
        for (Integer j : g.getOutputList(i)) {
            if (!g.isExplored(j)) {
                dfs(g, j);
            }
        }
        if (isFirstPass) {
            f.put(++t, i);
        }
    }

    public String searchStrongComponents(Digraph g) {
        isFirstPass = true;
        t = 0;
        f = new HashMap<>(g.size());
        g.setReversed(true);
        dfsLoop(g);
        g.clean();
        g.setReversed(false);
        s = 0;
        leader = new HashMap<>(g.size());
        isFirstPass = false;
        dfsLoop(g);
        List<Integer> scs = new LinkedList<>(leader.values());
        Collections.sort(scs);
        Iterator<Integer> it = ((Deque<Integer>) scs).descendingIterator();
        String result = "";
        for (int i = 0; i < 5; i++) {
            if (it.hasNext()) {
                result += "" + it.next();
            } else {
                result += "0";
            }
            if (i < 4) {
                result += ",";
            }
        }
        return result;
    }


    public static void main(String[] args) throws IOException {
        Digraph g = Util.readFromFile("SCC.txt");
        StrongComponentsSearcher scs = new StrongComponentsSearcher();
        long timeStart = System.currentTimeMillis();
        String result = scs.searchStrongComponents(g);
        long elTime = System.currentTimeMillis() - timeStart;
        System.out.println("result is: " + result + " computing time: " + elTime + " ms");
    }
}
