package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by akapitonov on 2/23/2015.
 */
public class Util {
    public static Digraph readFromFile(String fileName) throws IOException {
        List<String> tmp = null;

        try {
            tmp = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tmp == null || tmp.size() == 0) {
            throw new IOException("Empty file");
        }

        StringTokenizer st;
        final String DELIMITER = " ";
        st = new StringTokenizer(tmp.get(tmp.size() - 1), DELIMITER);
        int max = 0;
        for (String line : tmp) {
            st = new StringTokenizer(line, " ");
            int t = Integer.parseInt(st.nextToken());
            if (max < t) max = t;
        }
        Digraph digraph = new Digraph(max);
        for (int i = tmp.size() - 1; i >= 0; i--) {
            st = new StringTokenizer(tmp.get(i), DELIMITER);
            int vertexFrom = Integer.parseInt(st.nextToken());
            int vertexTo = Integer.parseInt(st.nextToken());
            digraph.getInputList(vertexTo).add(vertexFrom);
            digraph.getOutputList(vertexFrom).add(vertexTo);
        }

        return digraph;
    }

    public static Iterator<Integer> getDescendingIterator(Collection<Integer> collection) {
        List<Integer> deque = new LinkedList<>(collection);
        Collections.sort(deque);
        return ((Deque) deque).descendingIterator();
    }
}
