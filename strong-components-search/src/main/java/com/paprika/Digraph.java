package com.paprika;

import java.util.*;

/**
 * Created by akapitonov on 2/22/2015.
 */
public class Digraph {

    public class VertexData {
        private List<Integer> in;
        private List<Integer> out;
        private boolean isExplored;

        public VertexData() {
            this.in = new ArrayList<>();
            this.out = new ArrayList<>();
            this.isExplored = false;
        }

        public void addInputVertex(Integer fromVertex) {
            in.add(fromVertex);
        }

        public void addOutputVertex(Integer toVertex) {
            out.add(toVertex);
        }

        public List<Integer> getInList() {
            return this.in;
        }

        public List<Integer> getOutList() {
            return this.out;
        }

        public boolean isExplored() {
            return this.isExplored;
        }

        public void setExplored(boolean isExplored) {
            this.isExplored = isExplored;
        }
    }

    private Map<Integer, VertexData> graph;
    private boolean isReversed;

    public Digraph(int size) {
        graph = new HashMap<Integer, VertexData>(size);
        for (int i = 1; i <= size; i++) {
            graph.put(i, new VertexData());
        }
        isReversed = false;
    }

    public void addInput(int vertex, int fromVertex) {
        graph.get(vertex).addInputVertex(fromVertex);
    }

    public void addOutput(int vertex, int toVertex) {
        graph.get(vertex).addOutputVertex(toVertex);
    }

    public List<Integer> getInputList(int vertex) {
        return isReversed ? graph.get(vertex).getOutList() : graph.get(vertex).getInList();
    }

    public List<Integer> getOutputList(int vertex) {
        return isReversed ? graph.get(vertex).getInList() : graph.get(vertex).getOutList();
    }

    public boolean isExplored(int vertex) {
        return graph.get(vertex).isExplored();
    }

    public void setExplored(int vertex, boolean isExplored) {
        graph.get(vertex).setExplored(isExplored);
    }

    public int size() {
        return graph.size();
    }

    public void setReversed(boolean isReversed) {
        this.isReversed = isReversed;
    }

    public boolean isReversed() {
        return this.isReversed;
    }

    public void clean() {
        for (int i = 1; i <= graph.size(); i++) {
            graph.get(i).setExplored(false);
        }
    }

    public List<Integer> getVertexList() {
        List<Integer> result = new ArrayList<>(graph.size());
        result.addAll(graph.keySet());
        return result;
    }

    public Iterator<Integer> getBackwardVertexIterator() {
        return Util.getDescendingIterator(graph.keySet());
    }

    public VertexData getVertexData(int vertex) {
        return graph.get(vertex);
    }
}
