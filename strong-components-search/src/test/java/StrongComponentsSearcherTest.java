import com.paprika.Digraph;
import com.paprika.StrongComponentsSearcher;
import com.paprika.Util;
import org.junit.Assert;

/**
 * Created by akapitonov on 2/23/2015.
 */
public class StrongComponentsSearcherTest {
    @org.junit.Test
    public void testZero() throws Exception {
        Digraph g = Util.readFromFile("test0.txt");
        StrongComponentsSearcher scs = new StrongComponentsSearcher();
        Assert.assertEquals("3,3,3,0,0", scs.searchStrongComponents(g));
    }

    @org.junit.Test
    public void testOne() throws Exception {
        Digraph g = Util.readFromFile("test1.txt");
        StrongComponentsSearcher scs = new StrongComponentsSearcher();
        Assert.assertEquals("3,3,3,0,0", scs.searchStrongComponents(g));
    }

    @org.junit.Test
    public void testTwo() throws Exception {
        Digraph g = Util.readFromFile("test2.txt");
        StrongComponentsSearcher scs = new StrongComponentsSearcher();
        Assert.assertEquals("3,3,2,0,0", scs.searchStrongComponents(g));
    }

    @org.junit.Test
    public void testThree() throws Exception {
        Digraph g = Util.readFromFile("test3.txt");
        StrongComponentsSearcher scs = new StrongComponentsSearcher();
        Assert.assertEquals("3,2,2,2,1", scs.searchStrongComponents(g));
    }
}
