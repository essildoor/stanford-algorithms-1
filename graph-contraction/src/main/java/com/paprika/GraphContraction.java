package com.paprika;

import java.util.Collections;
import java.util.List;

/**
 * Created by akapitonov on 2/20/2015.
 */
public class GraphContraction {
    private static int contractAndGetCut(Graph g) {
        while (g.getSize() > 2) {
            List<Integer> list = g.getVertexIndices();
            Collections.shuffle(list);
            Integer v1 = list.get(0);
            list = g.getAdjacencyList(v1);
            Collections.shuffle(list);
            Integer v2 = list.get(0);
            g.merge(v1, v2);
        }
        return g.getEdgesNumber();
    }

    public static int findMinimumCut(Graph graph) throws CloneNotSupportedException {
        int m = graph.getEdgesNumber();
        int[] tmp = new int[m];
        for (int i = 0; i < m; i++) {
            tmp[i] = contractAndGetCut(graph.clone());
            System.out.println("min cut for " + i + " step is " + tmp[i]);
        }
        return min(tmp);
    }

    private static int min(int[] a) {
        int min = a[0];
        for (int i : a) {
            if (i < min) min = i;
        }
        return min;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Graph g = Util.readFromFile("kargerMinCut.txt");
        System.out.println("graph info:");
        System.out.println("vertexes number: " + g.getSize());
        System.out.println("edges number: " + g.getEdgesNumber());
        System.out.println("minimum cut is: " + findMinimumCut(g));
    }
}
