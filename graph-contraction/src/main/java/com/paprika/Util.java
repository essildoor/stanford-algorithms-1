package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by akapitonov on 2/20/2015.
 */
public class Util {

    public static Graph readFromFile(String fileName) {
        List<String> tmp = null;

        try {
            tmp = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tmp == null || tmp.size() == 0) {
            return new Graph();
        }

        Map<Integer, List<Integer>> g = new HashMap<>();
        StringTokenizer st;
        List<Integer> adjacentVertices;
        for (String line : tmp) {
            st = new StringTokenizer(line, " \t");
            Integer vertex = Integer.parseInt(st.nextToken());
            adjacentVertices = new ArrayList<>();
            while (st.hasMoreTokens()) {
                adjacentVertices.add(Integer.parseInt(st.nextToken()));
            }
            g.put(vertex, adjacentVertices);
        }

        return new Graph(g);
    }

    public static void printGraph(Graph g) {
        for (Integer vertex : g.getVertexIndices()) {
            System.out.print("\n" + vertex + " > ");
            for (Integer adjVert : g.getAdjacencyList(vertex)) {
                System.out.print(adjVert + " ");
            }
        }
        System.out.println("\n\n\n");
    }
}
