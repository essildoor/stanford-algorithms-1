package com.paprika;

import java.util.*;

/**
 * Created by akapitonov on 2/20/2015.
 */
public class Graph implements Cloneable {

    private Map<Integer, List<Integer>> graph;

    public Graph(Map<Integer, List<Integer>> graph) {
        this.graph = graph;
    }

    public Graph() {
    }

    public List<Integer> getVertexIndices() {
        List<Integer> result = new ArrayList<>(graph.size());
        result.addAll(graph.keySet());
        return result;
    }

    public List<Integer> getAdjacencyList(Integer vertexIndex) {
        List<Integer> result = new ArrayList<>();
        result.addAll(graph.get(vertexIndex));
        return result;
    }

    public int getSize() {
        return graph.size();
    }

    public int getEdgesNumber() {
        int result = 0;
        for (List<Integer> adjList : graph.values()) result += adjList.size();
        return result / 2;
    }

    public Graph clone() throws CloneNotSupportedException {
        super.clone();
        Map<Integer, List<Integer>> clonedGraph = new HashMap<>();
        for (Integer vertex : graph.keySet()) {
            List<Integer> clonedAdjList = new ArrayList<>();
            for (Integer adjVert : getAdjacencyList(vertex)) {
                clonedAdjList.add(new Integer(adjVert.intValue()));
            }
            clonedGraph.put(new Integer(vertex.intValue()), clonedAdjList);
        }
        return new Graph(clonedGraph);
    }

    /**
     * merge vertex with index v2 to vertex with index v1
     *
     * @param v1 vertex index to merge to
     * @param v2 vertex index to merge from
     */
    public void merge(int v1, int v2) {
        List<Integer> list1 = graph.get(v1);
        List<Integer> list2 = graph.get(v2);
        Iterator<Integer> it = list1.iterator();
        while (it.hasNext()) {
            Integer t = it.next();
            if (t.equals(v2)) {
                it.remove();
                break;
            }
        }
        it = list2.iterator();
        while (it.hasNext()) {
            Integer i = it.next();
            if (i.equals(v1)) {
                it.remove();
                break;
            }
        }
        list1.addAll(list2);
        graph.remove(v2);
        for (List<Integer> adjList : graph.values()) {
            updateAdjacencyList(adjList, v1, v2);
        }
        removeSelfLoops(v1);
    }

    private void updateAdjacencyList(List<Integer> adjList, Integer v1, Integer v2) {
        for (int i = 0; i < adjList.size(); i++) {
            if (v2.equals(adjList.get(i))) {
                adjList.set(i, v1);
            }
        }
    }

    private void removeSelfLoops(Integer v) {
        if (graph.get(v).contains(v)) {
            for (Iterator<Integer> it = graph.get(v).iterator(); it.hasNext();) {
                Integer i = it.next();
                if (v.equals(i)) {
                    it.remove();
                }
            }
        }
    }
}
