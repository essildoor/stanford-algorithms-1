package com.paprika;

import org.junit.Assert;

/**
 * Created by akapitonov on 2/20/2015.
 */
public class GraphContractionTest {

    @org.junit.Test
    public void testOne() throws Exception {
        Graph g = Util.readFromFile("simple.txt");
        Assert.assertEquals(2, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testTwo() throws Exception {
        Graph g = Util.readFromFile("test1.txt");
        Assert.assertEquals(2, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testThree() throws Exception {
        Graph g = Util.readFromFile("test2.txt");
        Assert.assertEquals(2, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testFour() throws Exception {
        Graph g = Util.readFromFile("test3.txt");
        Assert.assertEquals(1, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testFive() throws Exception {
        Graph g = Util.readFromFile("test4.txt");
        Assert.assertEquals(1, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testSix() throws Exception {
        Graph g = Util.readFromFile("test6.txt");
        Assert.assertEquals(2, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testSeven() throws Exception {
        Graph g = Util.readFromFile("test7.txt");
        Assert.assertEquals(2, GraphContraction.findMinimumCut(g));
    }

    @org.junit.Test
    public void testEight() throws Exception {
        Graph g = Util.readFromFile("test5.txt");
        Assert.assertEquals(3, GraphContraction.findMinimumCut(g));
    }
}
