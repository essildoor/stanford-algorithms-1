package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by akapitonov on 3/4/2015.
 */
public class Util {

    private static List<String> readFromFile(String fileName) {
        List<String> result = null;

        try {
            result = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result == null ? Collections.<String>emptyList() : result;
    }

    public static long[] readFromFileForSumOfTwo(String fileName) {
        List<String> fileContent = readFromFile(fileName);
        Set<Long> tmp = new TreeSet<>();

        for (String row : fileContent) {
            tmp.add(Long.parseLong(row));
        }

        long[] result = new long[tmp.size()];

        int i = 0;
        for (long l : tmp) {
            result[i++] = l;
        }

        return result;
    }
}