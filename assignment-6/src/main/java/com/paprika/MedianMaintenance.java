package com.paprika;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by akapitonov on 3/4/2015.
 */
public class MedianMaintenance {
    private MedianMaintenance() {}

    public static MedianMaintenance getInstance() {
        return new MedianMaintenance();
    }

    public long count(String fileName) {
        String line;
        long result = 0;
        Set<Long> storage = new TreeSet<>();
        try(
                InputStream fis = new FileInputStream(fileName);
                InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
                BufferedReader br = new BufferedReader(isr)
        ) {
            while ((line = br.readLine()) != null) {
                Long num = Long.parseLong(line);
                storage.add(num);
                result += getMedian(storage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private long getMedian(Set<Long> set) {
        final int medianIndex = set.size() % 2 == 0 ? set.size() / 2 - 1 : (set.size() + 1) / 2 - 1;
        Iterator<Long> it = set.iterator();
        for (int i = 0; i < medianIndex; i++) it.next();
        return it.next();
    }

    public static void main(String[] args) {
        System.out.println(getInstance().count("Median.txt"));
    }
}
