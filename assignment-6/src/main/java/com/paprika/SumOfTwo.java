package com.paprika;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;

/**
 * Singleton
 * <p/>
 * Created by akapitonov on 3/4/2015.
 */
public class SumOfTwo {

    private SumOfTwo() {
    }

    public static SumOfTwo getInstance() {
        return new SumOfTwo();
    }

    public long count(long[] source, long rangeStart, long rangeEnd) {
        Set<Long> resultSet = new HashSet<>();
        for (int i = 0; i < source.length - 1; i++) {
            int indexStart = getStartIndex(source, i, rangeStart);
            int indexEnd = getEndIndex(source, i, rangeEnd);
            if (indexStart < 0 || indexEnd < 0 || indexStart > indexEnd) continue;

            for (int j = indexStart; j <= indexEnd; j++) {
                resultSet.add(source[i] + source[j]);
            }
        }

        return resultSet.size();
    }

    private int getStartIndex(long[] array, int currentIndex, long rangeStart) {
        long t = array[currentIndex] + array[array.length - 1];
        if (t < rangeStart) return -1;
        final long border = rangeStart - array[currentIndex];
        int index = Arrays.binarySearch(array, border);
        if (index >= 0) {
            return index <= currentIndex ? currentIndex + 1 : index;
        }
        return -(index + 1);
    }

    private int getEndIndex(long[] array, int currentIndex, long rangeEnd) {
        long t = array[currentIndex] + array[currentIndex + 1];
        if (t > rangeEnd) return -1;
        final long border = rangeEnd - array[currentIndex];
        int index = Arrays.binarySearch(array, border);
        if (index >= 0) {
            return index;
        }
        return -(index + 2);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long time0 = System.currentTimeMillis();
        long[] source = Util.readFromFileForSumOfTwo("algo1-programming_prob-2sum.txt");
        long timeRead = System.currentTimeMillis() - time0;
        System.out.println("File read in " + timeRead + " ms");
        time0 = System.currentTimeMillis();
        long answer = getInstance().count(source, -10000, 10000);
        long timeCount = System.currentTimeMillis() - time0;
        System.out.println("Counted in " + timeCount + " ms");
        System.out.println("Answer is " + answer);
    }
}
