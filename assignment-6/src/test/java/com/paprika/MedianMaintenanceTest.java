package com.paprika;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by akapitonov on 3/4/2015.
 */
public class MedianMaintenanceTest {

    private MedianMaintenance medianMaintenance;

    @Before
    public void setUp() throws Exception {
        this.medianMaintenance = MedianMaintenance.getInstance();
    }

    @Test
    public void testOne() throws Exception {
        Assert.assertEquals(54, medianMaintenance.count("test2_1.txt"));
    }

    @Test
    public void testTwo() throws Exception {
        Assert.assertEquals(23, medianMaintenance.count("test2_2.txt"));
    }

    @Test
    public void testThree() throws Exception {
        Assert.assertEquals(55, medianMaintenance.count("test2_3.txt"));
    }

    @Test
    public void testFour() throws Exception {
        Assert.assertEquals(148, medianMaintenance.count("test2_4.txt"));
    }
}
