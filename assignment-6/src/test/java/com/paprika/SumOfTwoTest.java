package com.paprika;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by akapitonov on 3/4/2015.
 */
public class SumOfTwoTest {

    private static final int rangeStart = -10000;
    private static final int rangeEnd = 10000;

    private static final String file1 = "test1_1.txt";
    private static final String file2 = "test1_2.txt";
    private static final String file3 = "test1_3.txt";

    private SumOfTwo sumOfTwo;

    @Before
    public void setUp() throws Exception {
        this.sumOfTwo = SumOfTwo.getInstance();
    }

    @Test
    public void testOne() throws Exception {
        Assert.assertEquals(3, sumOfTwo.count(Util.readFromFileForSumOfTwo(file1), rangeStart, rangeEnd));
    }

    @Test
    public void testTwo() throws Exception {
        Assert.assertEquals(5, sumOfTwo.count(Util.readFromFileForSumOfTwo(file2), rangeStart, rangeEnd));
    }

    @Test
    public void testThree() throws Exception {
        Assert.assertEquals(6, sumOfTwo.count(Util.readFromFileForSumOfTwo(file3), rangeStart, rangeEnd));
    }
}
